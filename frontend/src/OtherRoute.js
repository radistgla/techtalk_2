import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";

const OtherRoute = () => {
  const [statusText, setStatusText] = useState("No response");

  const location = useLocation();

  useEffect(() => {
    const url = new URLSearchParams(location.search).get("url");

    if (!url) {
      setStatusText("No url");

      return;
    }

    const handleEffect = async () => {
      try {
        const response = await fetch(url);

        const text = await response.text();

        setStatusText(`Got response: ${text}`);
      } catch (e) {
        console.error(e);

        setStatusText(`Response ended with an error: ${e}`);
      }
    };

    handleEffect();
  }, [location]);

  return <>{statusText}</>;
};

export default OtherRoute;
